package com.twuc.webApp.service;

import com.twuc.webApp.dao.PersonaRepository;
import com.twuc.webApp.dao.ReservationRepository;
import com.twuc.webApp.entit.Persona;
import com.twuc.webApp.entit.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author tao.dong
 */
@Service
public class PersonaService {
    //TODO: field injection is not recommended
    @Autowired
    private PersonaRepository personaRepository;
    //TODO: field injection is not recommended
    @Autowired
    private ReservationRepository reservationRepository;
    public Persona createPersona(Persona persona) {
        return personaRepository.saveAndFlush(persona);
    }

    public Persona find(Long id) {
        Optional<Persona> res = personaRepository.findById(id);

        //TODO: 可以简化 orElse
        if (res.isPresent()){
            return res.get();
        }
        return null;
    }

    public List<Persona> find() {
        return personaRepository.findAll();
    }

    public void update(Persona persona) {
        personaRepository.saveAndFlush(persona);
    }

    public Reservation createReservation(Reservation reservation, Long personaId) {
        Persona persona = this.find(personaId);
        reservation.setPersona(persona);
        return reservationRepository.saveAndFlush(reservation);
    }
}
