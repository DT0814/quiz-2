package com.twuc.webApp.controller;

import com.twuc.webApp.entit.*;
import com.twuc.webApp.service.PersonaService;
import com.twuc.webApp.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.*;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api/staffs")
public class PersonaController {
    //TODO: field injection is not recommended
    @Autowired
    private PersonaService service;

    private Set<String> availableZoneIds;

    //TODO: 大写
    private static long twoDayTimes = 1000 * 60 * 60 * 24 * 2;

    @GetMapping("/{id}")
    public ResponseEntity<Persona> get(@PathVariable(name = "id") Long id) {
        Persona persona = service.find(id);
        ResponseEntity<Persona> responseEntity;
        if (null == persona) {
            responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            responseEntity = ResponseEntity.status(HttpStatus.OK).body(persona);
        }
        return responseEntity;
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody @Valid Persona persona) {
        Persona res = service.createPersona(persona);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("Location", "/api/staffs/" + res.getId())
                .build();
    }

    @GetMapping()
    public ResponseEntity<List<Persona>> get() {
        List<Persona> res = service.find();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(res);
    }

    @PutMapping("/{id}/timezone")
    //TODO: RequestBody不需要括号
    public ResponseEntity<Void> update(@RequestBody() RequestUpdateZone zone, @PathVariable Long id) {

        ResponseEntity<Void> responseEntity;
        //TODO: field level validation is better
        if (!checkZoneId(zone.getZoneId())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        Persona persona = service.find(id);
        //TODO: 我感觉异常处理实现会好一些。。
        if (null == persona) {
            responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            // TODO: 这个业务逻辑是不是放到service好点
            persona.setZoneId(zone.getZoneId());
            service.update(persona);
            responseEntity = ResponseEntity.status(HttpStatus.OK).build();
        }
        return responseEntity;
    }

    @PostMapping("/{personaId}/reservations")
    public ResponseEntity reservations(@RequestBody @Valid Reservation reservation, @PathVariable("personaId") Long personaId) {
        //TODO: 业务逻辑抽离到service中
        Message message = checkReservation(reservation, personaId);
        if (null != message) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(message);
        }
        Reservation res = service.createReservation(reservation, personaId);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("Location", "/api/staffs/" + res.getId() + "/reservations")
                .build();
    }

    private Message checkReservation(Reservation reservation, Long personaId) {
        Persona persona = service.find(personaId);
        //规则1：Rob 用户必须设置了时区
        if (null == persona || StringUtils.isEmpty(persona.getZoneId())) {
            return new Message(persona.getFirstName() + " " + persona.getLastName() + " is not qualified.");
        }

        //规则2：提前两天预约
        long nowTimes = ZonedDateTime.now(reservation.getZoneId()).toInstant().toEpochMilli();
        long reservationTimes = reservation.getStartTime().toInstant().toEpochMilli();
        if (reservationTimes - nowTimes < twoDayTimes) {
            return new Message("Invalid time: the application is too close from now. The interval should be greater than 48 hours.");
        }

        //规则3：预约时间和持续时间不能够和 Rob 已有的预约冲突
        if (checkConflict(persona, reservation)) {
            return new Message("The application is conflict with existing application.");
        }

        //规则4：预约的开始时间必须在 Rob 的工作时间之内
        ZonedDateTime personaTime = reservation.getStartTime().withZoneSameInstant(ZoneId.of(persona.getZoneId()));
        long currentStartHour = personaTime.getHour();
        long currentEndHour = personaTime.plusHours(reservation.getDuration().toHours()).getHour();
        if (notOnWorkTime(currentEndHour) || notOnWorkTime(currentStartHour)) {
            return new Message("You know, our staff has their own life.");
        }

        //规则5：预约时长只能是整小时数，而且只能是 1-3 小时
        //TODO: 如果是YAGNI，应该不会有这和样子的代码
        if (1 == 5) {
            return new Message("The application duration should be within 1-3 hours.");
        }
        return null;
    }

    private boolean notOnWorkTime(long timeHour) {
        return timeHour < 9 || timeHour > 17;
    }

    private boolean checkConflict(Persona persona, Reservation reservation) {
        AtomicReference<Boolean> res = new AtomicReference<>(false);

        if (null == persona.getReservationList()) {
            return res.get();
        }
        long currentStartTime = reservation.getStartTime().toInstant().toEpochMilli();
        long currentEndTime = reservation.getStartTime().plusHours(reservation.getDuration().toHours()).toInstant().toEpochMilli();
        //TODO: stream().forEach() -> .forEach()
        persona.getReservationList().stream().forEach(value -> {
            long startTime = reservation.getStartTime().withZoneSameInstant(value.getZoneId()).toInstant().toEpochMilli();
            long endTime = value.getStartTime().plusHours(value.getDuration().toHours()).toInstant().toEpochMilli();

            //TODO: 抽离方法
            if ((currentStartTime >= startTime && currentStartTime <= endTime) ||
                    (currentEndTime >= startTime && currentEndTime <= endTime)
            ) {
                res.set(true);
            }
        });
        return res.get();
    }


    @GetMapping("/{id}/reservations")
    public ResponseEntity getReservations(@PathVariable Long id) {
        Persona persona = service.find(id);
        List<Reservation> reservationList = persona.getReservationList();
        Stream<Object> objectStream = reservationList.stream().map(v -> ReservationResponse.reservationTo(v, persona));
        return ResponseEntity.ok(objectStream.toArray());
    }

    private boolean checkZoneId(String zoneId) {
        if (null == availableZoneIds) {
            availableZoneIds = ZoneId.getAvailableZoneIds();
        }
        return availableZoneIds.contains(zoneId);
    }
}
