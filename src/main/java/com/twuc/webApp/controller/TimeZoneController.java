package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;
import java.util.Set;


//TODO: 配置idea，去掉这些@author，项目中不会写这些。
/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api/timezones")
public class TimeZoneController {
    private Set<String> availableZoneIds;

    @GetMapping()
    public ResponseEntity<String[]> get() {
        if (null == availableZoneIds) {
            //TODO: readme: ZoneRulesProvider.getAvailableZoneIds()
            availableZoneIds = ZoneId.getAvailableZoneIds();
        }
        return ResponseEntity.status(HttpStatus.OK).body(availableZoneIds.toArray(new String[0]));
    }
}
