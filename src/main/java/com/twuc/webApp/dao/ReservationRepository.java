package com.twuc.webApp.dao;

import com.twuc.webApp.entit.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author tao.dong
 */
public interface ReservationRepository extends JpaRepository<Reservation,Long> {
}
