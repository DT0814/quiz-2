package com.twuc.webApp.dao;

import com.twuc.webApp.entit.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author tao.dong
 */
public interface PersonaRepository extends JpaRepository<Persona,Long> {
}
