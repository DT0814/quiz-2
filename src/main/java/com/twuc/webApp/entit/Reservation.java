package com.twuc.webApp.entit;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.time.DurationMax;
import org.hibernate.validator.constraints.time.DurationMin;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Duration;
//TODO: unused import
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * @author tao.dong
 */
@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 128)
    @Length(max = 128)
    @NotNull
    private String username;

    @Column(length = 64)
    @Length(max = 64)
    @NotNull
    private String companyName;

    @Column(length = 128)
    @NotNull
    private ZoneId zoneId;

    @Column(length = 64)
    @NotNull
    @DateTimeFormat
    private ZonedDateTime startTime;

    @Column(length = 5)
    @NotNull
    @DurationMax(hours = 3, message = "The application duration should be within 1-3 hours.")
    @DurationMin(hours = 1, message = "The application duration should be within 1-3 hours.")
    private Duration duration;

    @ManyToOne
    @JoinColumn(name = "persona_id")
    private Persona persona;

    //TODO: 如果你用了tdd，或者考虑到YAGNI，会后写这些validation，然后你自动生成的constructor就不会有这个inline annotation
    public Reservation(@Length(max = 128) @NotNull String username, @Length(max = 64) @NotNull String companyName, @NotNull ZoneId zoneId, @NotNull ZonedDateTime startTime, @NotNull Duration duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public Reservation() {
    }


    //TODO: setter no need
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public void setZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
}
