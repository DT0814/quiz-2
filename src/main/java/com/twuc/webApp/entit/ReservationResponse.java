package com.twuc.webApp.entit;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ReservationResponse {
    private String username;
    private String companyName;
    private Duration duration;
    private StartTime startTime;

    public ReservationResponse() {
    }

    public static Object reservationTo(Reservation reservation, Persona persona) {
        //TODO: 有点复杂。。。
        ReservationResponse reservationResponse = new ReservationResponse();
        reservationResponse.setUsername(reservation.getUsername());
        reservationResponse.setDuration(reservation.getDuration());
        reservationResponse.setCompanyName(reservation.getCompanyName());

        StartTime startTime = new StartTime();

        TimeResponse client = new TimeResponse();
        client.setStartTime(reservation.getStartTime());
        client.setZoneId(reservation.getZoneId());

        TimeResponse staff = new TimeResponse();
        ZoneId zoneId = ZoneId.of(persona.getZoneId());
        staff.setZoneId(zoneId);

        ZonedDateTime destZonedDateTime = reservation.getStartTime().withZoneSameInstant(ZoneId.of(persona.getZoneId()));
        staff.setStartTime(destZonedDateTime);

        startTime.setClient(client);
        startTime.setStaff(staff);

        reservationResponse.setStartTime(startTime);
        return reservationResponse;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public StartTime getStartTime() {
        return startTime;
    }

    public void setStartTime(StartTime startTime) {
        this.startTime = startTime;
    }
}
