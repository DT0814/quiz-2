package com.twuc.webApp.entit;


//TODO: unused imports
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TimeResponse {

    private ZoneId zoneId;
    private ZonedDateTime startTime;

    public ZoneId getZoneId() {
        return zoneId;
    }

    public void setZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }
}
