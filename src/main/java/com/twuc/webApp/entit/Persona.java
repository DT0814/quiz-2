package com.twuc.webApp.entit;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author tao.dong
 */
@Entity
@Table(name = "staff")
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Length(max = 64)
    @Column(nullable = false)
    private String firstName;

    @NotNull
    @Length(max = 64)
    @Column(nullable = false)
    private String lastName;

    @Column()
    @Length(max = 64)
    private String zoneId;

    @OneToMany
    @JoinColumn(name = "persona_id")
    @JsonIgnore
    //TODO: 得给个初始值，有NullPointerException
    private List<Reservation> reservationList;

    public Persona() {
    }

    public Persona(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public List<Reservation> getReservationList() {
        return reservationList;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
