package com.twuc.webApp.entit;

public class StartTime {

    private TimeResponse client;
    private TimeResponse staff;

    public TimeResponse getClient() {
        return client;
    }

    public void setClient(TimeResponse client) {
        this.client = client;
    }

    public TimeResponse getStaff() {
        return staff;
    }

    public void setStaff(TimeResponse staff) {
        this.staff = staff;
    }
}
