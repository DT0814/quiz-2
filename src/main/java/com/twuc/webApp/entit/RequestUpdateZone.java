package com.twuc.webApp.entit;

/**
 * @author tao.dong
 */
public class RequestUpdateZone {
    private String zoneId;

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
