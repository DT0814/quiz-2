CREATE TABLE IF NOT EXISTS reservation
(
    `id`           BIGINT AUTO_INCREMENT PRIMARY KEY,
    `username`     VARCHAR(128) NOT NULL,
    `company_name` VARCHAR(64)  NOT NULL,
    `zone_id`      VARCHAR(128) NOT NULL,
    `start_time`   DATETIME     NOT NULL,
    `duration`     VARCHAR(20)   NOT NULL,
    `persona_id`   BIGINT       NOT NULL
);
alter table reservation
    add constraint FK_ID foreign key (persona_id) REFERENCES staff (`id`);
