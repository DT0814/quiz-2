package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import java.time.ZoneId;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

public class TimeZoneControllerTest extends ApiTestBase {

    @Test
    void test_getAll() throws Exception {
        //Arrange

        //Act
        mockMvc.perform(get("/api/timezones")).andExpect(content().string(serialize(ZoneId.getAvailableZoneIds())));
        //Assert
    }
}
