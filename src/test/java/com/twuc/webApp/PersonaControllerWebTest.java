package com.twuc.webApp;

import com.twuc.webApp.controller.PersonaController;
import com.twuc.webApp.dao.PersonaRepository;
import com.twuc.webApp.entit.Message;
import com.twuc.webApp.entit.Persona;
import com.twuc.webApp.entit.RequestUpdateZone;
import com.twuc.webApp.entit.Reservation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
//TODO: unused imports
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PersonaControllerWebTest extends ApiTestBase {
    @Autowired
    PersonaRepository repository;
    @Autowired
    PersonaController controller;

    @Test
    void should_be_return_status_201_and_location_return_right() throws Exception {
        //Arrange
        Persona persona = new Persona("Rob", "Hall");
        MockHttpServletRequestBuilder requestBuilder = post("/api/staffs")
                .content(serialize(persona))
                .contentType(MediaType.APPLICATION_JSON);
        //Act
        //Assert
        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));
    }

    @Test
    void should_be_400_because_last_and_first_not_right() throws Exception {
        //Arrange
        Persona persona = new Persona(null, "Hall");
        MockHttpServletRequestBuilder requestBuilder = post("/api/staffs")
                .content(serialize(persona))
                .contentType(MediaType.APPLICATION_JSON);

        //Act
        //Assert
        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());
        requestBuilder.content(serialize(new Persona("ka", "12345678901234567890123456789012345678901234567890123456789012345")));
        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_be_return_persona() {
        //Arrange
        Persona persona = new Persona("Rob", "Hall");
        repository.saveAndFlush(persona);
        //Act
        ResponseEntity<Persona> response = controller.get(persona.getId());
        //Assert
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals("Rob", response.getBody().getFirstName());
        Assertions.assertEquals("Hall", response.getBody().getLastName());
    }

    @Test
    void should_be_return_not_found() {
        //Arrange
        //Act
        ResponseEntity<Persona> response = controller.get(200L);
        //Assert
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

    }

    @Test
    void should_be_return_all_persona() {
        //Arrange
        Persona persona = new Persona("Rob", "Hall");
        Persona persona2 = new Persona("Rob", "Hall");
        repository.saveAndFlush(persona);
        repository.saveAndFlush(persona2);
        List<Persona> list = Arrays.asList(persona, persona2);
        //Act
        ResponseEntity<List<Persona>> response = controller.get();
        //Assert
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertArrayEquals(list.toArray(), response.getBody().toArray());
    }

    @Test
    void should_be_return_200_update_zoneId() {
        //Arrange
        Persona persona = new Persona("Rob", "Hall");
        repository.saveAndFlush(persona);
        RequestUpdateZone requestUpdateZone = new RequestUpdateZone();
        requestUpdateZone.setZoneId("Asia/Chongqing");
        //Act
        ResponseEntity response = controller.update(requestUpdateZone, 1L);
        //Assert
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void should_be_return_200_update_zoneId_give_a_not_zone() {
        //Arrange
        Persona persona = new Persona("Rob", "Hall");
        repository.saveAndFlush(persona);
        RequestUpdateZone requestUpdateZone = new RequestUpdateZone();
        requestUpdateZone.setZoneId("error_zone");
        //Act
        ResponseEntity response = controller.update(requestUpdateZone, 1L);
        //Assert
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void should_be_update_zoneId_return_zone() {
        //Arrange
        Persona persona = new Persona("Rob", "Hall");
        repository.saveAndFlush(persona);
        RequestUpdateZone requestUpdateZone = new RequestUpdateZone();
        requestUpdateZone.setZoneId("Asia/Chongqing");
        controller.update(requestUpdateZone, 1L);
        //Act
        ResponseEntity<Persona> responseEntity = controller.get(persona.getId());
        //Assert
        Assertions.assertEquals("Asia/Chongqing", responseEntity.getBody().getZoneId());
    }

    @Test
    void should_be_return_201_by_create_reservation() {
        //Arrange
        Persona persona = new Persona("Rob", "Hall");
        repository.saveAndFlush(persona);
        RequestUpdateZone requestUpdateZone = new RequestUpdateZone();
        requestUpdateZone.setZoneId("Asia/Chongqing");
        controller.update(requestUpdateZone, persona.getId());
        Reservation reservation = new Reservation("Sofia", "ThoughtWorks", ZoneId.of("Africa/Nairobi"), ZonedDateTime.now().withHour(10).plusDays(3), Duration.parse("PT1H"));
        //Act

        ResponseEntity<Persona> responseEntity = controller.reservations(reservation, persona.getId());
        //Assert
        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assertions.assertEquals("/api/staffs/" + reservation.getId() + "/reservations", responseEntity.getHeaders().getFirst("Location"));

    }

    @Test
    void should_be_return_400_by_create_reservation() throws Exception {
        //Arrange
        Reservation reservation = new Reservation(null, "ThoughtWorks", ZoneId.of("Africa/Nairobi"), ZonedDateTime.now().withHour(10).plusDays(3), Duration.parse("PT1H"));
        //Act
        MockHttpServletRequestBuilder requestBuilder = post("/api/staffs/1/reservations")
                .content(serialize(reservation))
                .contentType(MediaType.APPLICATION_JSON);
        //Assert
        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());
        requestBuilder.content(serialize(new Reservation("123", "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", ZoneId.of("Africa/Nairobi"),ZonedDateTime.parse("2019-08-20T11:46:00+03:00") , Duration.parse("PT1H"))));
        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_be_return_409_by_create_reservation_when_startTime_not_then_two_days() throws Exception {
        Persona persona = new Persona("Rob", "Hall");
        repository.saveAndFlush(persona);
        RequestUpdateZone requestUpdateZone = new RequestUpdateZone();
        requestUpdateZone.setZoneId("Asia/Chongqing");
        controller.update(requestUpdateZone, persona.getId());
        //Arrange
        Reservation reservation = new Reservation("Sofia", "ThoughtWorks", ZoneId.of("Africa/Nairobi"), ZonedDateTime.now().withHour(10).plusDays(1), Duration.parse("PT1H"));
        //Act
        ResponseEntity responseEntity = controller.reservations(reservation, persona.getId());
        //Assert
        Assertions.assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
        Assertions.assertEquals(new Message("Invalid time: the application is too close from now. The interval should be greater than 48 hours."), responseEntity.getBody());
    }

    @Test
    void should_be_return_409_by_create_reservation_when_persona_not_have_zone() throws Exception {
        Persona persona = new Persona("Rob", "Hall");
        repository.saveAndFlush(persona);
        //Arrange
        Reservation reservation = new Reservation("Sofia", "ThoughtWorks", ZoneId.of("Africa/Nairobi"), ZonedDateTime.now().withHour(10).plusDays(3), Duration.parse("PT1H"));
        //Act
        ResponseEntity responseEntity = controller.reservations(reservation, persona.getId());
        //Assert
        Assertions.assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
        Assertions.assertEquals(new Message("Rob Hall is not qualified."), responseEntity.getBody());
    }

    @Test
    void should_be_return_409_by_create_reservation_when_time_conflict() throws Exception {
        //Arrange
        Persona persona = new Persona("Rob", "Hall");
        repository.saveAndFlush(persona);
        RequestUpdateZone requestUpdateZone = new RequestUpdateZone();
        requestUpdateZone.setZoneId("Asia/Chongqing");
        controller.update(requestUpdateZone, persona.getId());
        Reservation reservation = new Reservation("Sofia", "ThoughtWorks", ZoneId.of("Africa/Nairobi"), ZonedDateTime.now().withHour(8).plusDays(3), Duration.parse("PT1H"));

        //Act
        ResponseEntity responseEntity = controller.reservations(reservation, persona.getId());

        //Assert
        Assertions.assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
    }

    @Test
    void name() {
        ZoneId of = ZoneId.of("Africa/Nairobi");
        ZonedDateTime now = ZonedDateTime.now(of);

        //TODO: ???? excuse me???
        System.out.println(ZonedDateTime.now().withHour(1));
    }
}
